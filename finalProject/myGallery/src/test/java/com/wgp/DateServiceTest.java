package com.wgp;

import com.wgp.web.services.DateService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class DateServiceTest {
    private final DateService dateService = new DateService();


    @Test
    public void dateTest(){
        Date date = dateService.date();
        Assertions.assertNotNull(date);
    }


}
