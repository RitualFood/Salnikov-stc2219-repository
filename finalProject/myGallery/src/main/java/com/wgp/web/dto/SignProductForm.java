package com.wgp.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SignProductForm {

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    private String previewImageId;

    private String dateOfCreated;
}
