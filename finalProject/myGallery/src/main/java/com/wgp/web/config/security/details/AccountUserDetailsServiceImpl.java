package com.wgp.web.config.security.details;

import com.wgp.web.repositories.AccountsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class AccountUserDetailsServiceImpl implements UserDetailsService {

    private final AccountsRepository accountsRepository;

    @Override    // UserDetails это оболочка над моделью
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return new AccountUserDetailsImpl(
                accountsRepository.findByEmail(email)
                        .orElseThrow(() -> new UsernameNotFoundException("User not found / пользователь не найден") //orElseThrowроверяет на пустое значение
                        )
        );
    }
}
