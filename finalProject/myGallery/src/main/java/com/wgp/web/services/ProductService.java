package com.wgp.web.services;

import com.wgp.web.models.Account;
import com.wgp.web.models.Order;
import com.wgp.web.models.Product;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import com.wgp.web.repositories.ProductRepository;

import java.util.List;


@Service
@RequiredArgsConstructor
public class ProductService    {
    private final ProductRepository productRepository;


    public List<Product> listProducts() {

            return productRepository.findAll();

    }

    public void deleteProduct(Long id) {

        productRepository.deleteById(id);
    }


    public Product getProductById(Long id) {
        return productRepository.findById(id).orElse(null);
    }

    public void orderProduct(Long id) {

    //   Order order = Order.builder()
    //           .idProduct(id)
    //           .idAccount()
    //           .build();

    //   productRepository.orderById(id);
    }

}