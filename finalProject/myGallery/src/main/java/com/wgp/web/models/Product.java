package com.wgp.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Table(name = "products")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "title") //название
    private String title;

    @Column(name = "description", columnDefinition = "text") //описание
    private String description;



  // @OneToMany(cascade = CascadeType.ALL,
  //             fetch = FetchType.LAZY,
  //             mappedBy = "product")
  // private List<Image> images = new ArrayList<>();

    private Long previewImageId;


    private LocalDateTime dateOfCreated;

    @Column
    private String url;

    @PrePersist
    private void init() {

        dateOfCreated = LocalDateTime.now();
    }

 // public void addImageToProduct(Image image) {
 //     image.setProduct(this);
 //     images.add(image);
 // }
}
