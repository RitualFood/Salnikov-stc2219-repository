package com.wgp.web.controllers;

import com.wgp.web.dto.SignUpForm;
import com.wgp.web.dto.SignProductForm;
import com.wgp.web.services.SignProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;


@Controller
@RequiredArgsConstructor
public class SignProductController {

    private final SignProductService signProductService;

    @GetMapping("/signProduct")
    public String getSignUpPage(Model model, Authentication authentication) {
        if (authentication != null) {
            return "/SignProduct";
        }
        model.addAttribute( "SignProduct", new SignProductForm());
        return "redirect:/";
    }

    @PostMapping("/signProduct")
    public String signUp(@Valid SignProductForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(form);
            return "signProduct";
        }
        signProductService.signProduct(form);
        return "redirect:/signProduct";
    }
}