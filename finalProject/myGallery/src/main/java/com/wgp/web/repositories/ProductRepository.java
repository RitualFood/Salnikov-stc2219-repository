package com.wgp.web.repositories;


import com.wgp.web.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {


   // void orderById(Long id);
}
