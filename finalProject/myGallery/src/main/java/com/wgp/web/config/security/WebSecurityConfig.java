package com.wgp.web.config.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration                           // что использовать за место него в будущем? гугл...
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable(); // эту штуку надо в будущем сделать для защиты


        http.authorizeRequests()
                .antMatchers("/index").permitAll()
                .antMatchers("/signUp").permitAll()
                .antMatchers("/profile").authenticated()
                .antMatchers("/product-info").authenticated()
                .antMatchers("/").permitAll()
                .antMatchers("/signProduct").authenticated()
                .antMatchers("/accounts/**").hasAuthority("ADMIN")
                .and()
                .formLogin()
                .loginPage("/signIn") // страница логирования
                .usernameParameter("email") // парам стр авторизации
                .passwordParameter("password") // парам стр авторизации
                .defaultSuccessUrl("/profile") // куда перейдем если прошли авторизацию
                .failureUrl("/signIn?error")
                .permitAll()
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/signIn?logout")
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true);
    }
}
