package com.wgp.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.security.PermitAll;

@Controller
public class RootController {

    // PermitAll Определяет, что всем ролям безопасности позволяют вызвать указанный метод (ы) то есть что указанный метод (ы) "непроверен".
    // Это может быть определено на class или на методах. Определение этого на class означает, что применяется ко всем методам class.
    // Если определено на уровне метода, это только влияет на тот метод. Если RolesAllowed определяется на уровне class,
    // и эта аннотация применяется на уровне метода, аннотация PermitAll переопределяет RolesAllowed для указанного метода.
    @PermitAll
    @GetMapping("/")
    public String getRootPage() {
        return "signIn";
    }


}
