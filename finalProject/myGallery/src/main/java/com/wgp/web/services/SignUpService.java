package com.wgp.web.services;

import com.wgp.web.dto.SignUpForm;

public interface SignUpService {

    void signUp(SignUpForm form);
}
