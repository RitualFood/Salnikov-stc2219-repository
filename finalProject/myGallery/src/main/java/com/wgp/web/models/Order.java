package com.wgp.web.models;

import com.wgp.web.services.DateService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Table(name = "order")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Order {


    public enum State {
        NOT_PROCESSED, PROCESSED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_account") //
    private Long idAccount;

    @Column(name = "id_product") //
    private Long idProduct;


    private Date dateOfCreated;


    @PrePersist
    private void init() {

        DateService dateService = new DateService();
        dateOfCreated =  dateService.date();
    }


}


