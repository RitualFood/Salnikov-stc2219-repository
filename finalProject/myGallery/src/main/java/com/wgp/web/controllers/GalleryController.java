package com.wgp.web.controllers;

import com.wgp.web.models.Account;
import com.wgp.web.models.Product;
import com.wgp.web.repositories.AccountsRepository;
import com.wgp.web.repositories.ProductRepository;

import com.wgp.web.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


import java.io.IOException;
import java.security.Principal;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RequiredArgsConstructor
@Controller
public class GalleryController {

     private final ProductService productService;// ProductRepository  productService;

    //  private final AccountsRepository accountsRepository;// ProductRepository  productService;

    @GetMapping("/gallery")
    public String products(Model model, Authentication authentication) {

        List<Product> products = productService.listProducts();

         if (authentication != null) {
             model.addAttribute("user", true);
             if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ADMIN"))) {
                 model.addAttribute("admin", true);
             } else {
                 model.addAttribute("admin", false);
             }
         }else {
             model.addAttribute("user", false);
             model.addAttribute("admin", false);
         }
        model.addAttribute("products", products);
        return "gallery";
    }




    @PostMapping("/gallery/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Long ProductId) {
        productService.deleteProduct(ProductId);
        return "redirect:/";
    }


    @PostMapping("/gallery/{id}/edit")
    public String produtInfo(@PathVariable Long id, Model model, Authentication authentication) {
        Product product = productService.getProductById(id);
        model.addAttribute("product", product);

        if (authentication != null) {
            model.addAttribute("user", true);
            if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ADMIN"))) {
                model.addAttribute("admin", true);
            } else {
                model.addAttribute("admin", false);
            }
        }else {
            model.addAttribute("user", false);
            model.addAttribute("admin", false);
        }

        return "product-info";
    }

    //  @GetMapping("/product/{id}")
    //  public String produtInfo(@PathVariable Long id, Model model) {
    //      Product product = productRepository.getProductById(id);
    //      model.addAttribute("product", product);
//
//
    //      return "product-info";
    //  }

//    @PostMapping("/products/{product-id}/order")
//    public String orderProduct(@PathVariable("product-id") Long ProductId, Authentication authentication) {
//
//   //    authentication.
//   //     AccountsRepository details = (AccountsRepository) authentication.getPrincipal();
//   //     Account user = accountsRepository.getReferenceById(details.getShopUser().getId());
//   //     user.getCart().add(new Product(productId));
//   //    productService.orderProduct(ProductId);
//   //    return "redirect:/";
//    }
}
