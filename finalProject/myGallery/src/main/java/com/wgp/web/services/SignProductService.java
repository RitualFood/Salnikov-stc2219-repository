package com.wgp.web.services;

import com.wgp.web.dto.SignProductForm;

public interface SignProductService {

    void signProduct(SignProductForm form);
}
