package com.wgp.web.services;

import com.wgp.web.dto.SignProductForm;
import com.wgp.web.models.Product;
import com.wgp.web.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SignProductFormImpl implements SignProductService{

    private final ProductRepository productRepository;

    @Override
    public void signProduct(SignProductForm form) {
        Product product = Product.builder()
                .title(form.getTitle())
                .description(form.getDescription())
                .build();

        productRepository.save(product);
    }

    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    } //ps тут вопрос, стоит ли сразу удалять или удалять через логическое удаление ?
}
