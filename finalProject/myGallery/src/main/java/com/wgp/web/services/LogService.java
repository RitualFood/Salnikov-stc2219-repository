package com.wgp.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LogService {

    private final DateService dateService;

    public void sendToConsole(String text) {

        System.out.println(dateService.date() + " " + text);
    }
}
