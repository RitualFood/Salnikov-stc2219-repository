package com.wgp.web.services;

import com.wgp.web.dto.AccountDto;
import com.wgp.web.dto.SignUpForm;

import java.util.List;

public interface AccountService {

    List<AccountDto> getAllAccounts();

    void createAccount(SignUpForm dto);

    void deleteAccount(Long accountId);
}
